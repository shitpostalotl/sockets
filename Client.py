import socket, sys

# Create a TCP/IP socket
stream_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Define host and port
host = input("The following is the localhoast. It will be defaulted to if you do not imput a host: "+socket.gethostbyaddr(socket.gethostname())[0]+"\nInput host: ")
if host == "":
    host = socket.gethostbyaddr(socket.gethostname())[0]
port = 8080

# Connect the socket to the port where the server is listening
server_address = ((host, port))

print ("connecting")

stream_socket.connect(server_address)

# Send data
message = 'message'
stream_socket.sendall(message.encode('utf-8'))

# response
data = stream_socket.recv(10)
print (data)

input()

# close socket
stream_socket.close()