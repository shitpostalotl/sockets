import socket, sys

# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Ask for host and define host and port
host = input("The following is the localhoast. It will be defaulted to if you do not imput a host: "+socket.gethostbyaddr(socket.gethostname())[0]+"\nInput host: ")
if host == "":
    host = socket.gethostbyaddr(socket.gethostname())[0]
port = 8080

# Bind the socket to the port
sock.bind((host, port))
# Listen for incoming connections
sock.listen(1)

# Wait for a connection
print ('waiting for a connection')
connection, client = sock.accept()

print (client, 'connected')

# Receive the data in small chunks and retransmit it
data = connection.recv(16)
print ("received: "+data.decode("utf-8"))
if data:
    connection.sendall(data)
else:
    print ('no data from', client)

input()

# Close the connection
connection.close()